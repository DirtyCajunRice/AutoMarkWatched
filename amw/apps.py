from django.apps import AppConfig


class AmwConfig(AppConfig):
    name = 'amw'
    verbose_name = 'Auto Mark Watched'
